<?php

class Product {

    public static function All() {

        //Connect to a database
        $database = new Database();

        //Select all information about the products
        $query = "SELECT SKUName.SKU, Name, Price, Type, Size, Weigth, Width, Heigth, Depth FROM SKUName";
        $joinPrice = " LEFT JOIN SKUPrice ON SKUPrice.SKU = SKUName.SKU";
        $joinType = " LEFT JOIN SKUType ON SKUType.SKU = SKUName.SKU";
        $joinSize = " LEFT JOIN SKUSize ON SKUSize.SKU = SKUName.SKU";
        $joinWeigth = " LEFT JOIN SKUWeigth ON SKUWeigth.SKU = SKUName.SKU";
        $joinDimensions = " LEFT JOIN SKUDimensions ON SKUDimensions.SKU = SKUName.SKU";
        $order = " ORDER BY Type, SKU";

        $query = $query . $joinPrice . $joinType . $joinSize . $joinWeigth . $joinDimensions . $order;

        //Return query results
        return $database->query($query);
    }

    public static function AddType($data) {

        $database = new Database();

        $SKU = $database->escapeString($data['SKU']);
        $type = $database->escapeString($data['type']);

        $query = 'INSERT INTO SKUType VALUES(' . $SKU . ',' . $type . ')';
        $database->query($query);
    }

    public static function AddName($data) {

        $database = new Database();

        $SKU = $database->escapeString($data['SKU']);
        $name = $database->escapeString($data['name']);

        $query = 'INSERT INTO SKUName VALUES(' . $SKU . ',' . $name . ')';
        $database->query($query);
    }

    public static function AddPrice($data) {

        $database = new Database();

        $SKU = $database->escapeString($data['SKU']);
        $price = $database->escapeString($data['price']);

        $query = 'INSERT INTO SKUPrice VALUES(' . $SKU . ',' . $price . ')';
        $database->query($query);
    }

    public static function AddSize($data) {

        $database = new Database();

        $SKU = $database->escapeString($data['SKU']);
        $size = $database->escapeString($data['size']);

        $query = 'INSERT INTO SKUSize VALUES(' . $SKU . ',' . $size . ')';
        $database->query($query);
    }

    public static function AddDimensions($data) {

        $database = new Database();

        $SKU = $database->escapeString($data['SKU']);
        $w = $database->escapeString($data['width']);
        $h = $database->escapeString($data['heigth']);
        $d = $database->escapeString($data['depth']);

        $query = 'INSERT INTO SKUDimensions VALUES(' . $SKU . ',' . $w . ',' . $h . ',' . $d . ')';
        $database->query($query);
    }

    public static function AddWeigth($data) {

        $database = new Database();

        $SKU = $database->escapeString($data['SKU']);
        $weigth = $database->escapeString($data['weigth']);

        $query = 'INSERT INTO SKUWeigth VALUES(' . $SKU . ',' . $weigth . ')';
        $database->query($query);
    }

}
