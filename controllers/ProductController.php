<?php

class ProductController {

    public static function index() {
        $model = new Product();
        $products = $model->All();
        include 'views/list.php';
    }

    public static function new() {
        include 'views/new.php';
    }

    public static function create() {
        include 'views/new.php';
    }
    
    public static function delete() {
        include 'views/dump.php';
    }
}

?>