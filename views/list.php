<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>List of all products</title>
        <!-- BOOTSTRAP CSS-->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    </head>
    <body>
        <nav class="navbar navbar-light bg-light">
            <div class="container">
                <a class="navbar-brand">Products web page</a>
                <form class="form-inline">
                    <select class="form-control mr-sm-2">
                        <option value="massDelete">Mass delete action</option>
                    </select>
                    <button id="apply" class="btn btn-outline-danger my-2 my-sm-0" type="button">Apply</button>
                </form>
            </div>
        </nav>
        <div class="container pt-3">
            <form id="form" method="post" action="./delete">
                <div class="row d-flex">
                    
                    <?php
                    foreach ($products as $product) {
                        echo '<div class = "float-left col-md-3 col-sm-6">
                            <figure class = "card card-product">
                                <div class="ml-1 custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" name="' . $product['SKU'] . '" id="' . $product['SKU'] . '">
                                    <label class="custom-control-label" for="' . $product['SKU'] . '"></label>
                                 </div>
                                 <div class="name-wrap">
                                    <span class="SKU d-flex justify-content-center">' . $product['SKU'] . '</span>
                                 </div>
                                 <a href="" class = "title d-flex justify-content-center">' . $product['Name'] . '</a>
                                 <div class="price-wrap">
                                    <span class="price d-flex justify-content-center">€' . $product['Price'] . '</span>
                                 </div>
                            </figure>
                        </div>';
                    }
                    ?>
                    
                </div>
            </form>
        </div>

        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
        <!-- Optional JavaScript -->
        <script src="../js/massDelete.js"></script>
    </body>

</html>