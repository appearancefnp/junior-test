<?php

class Database {

    private $servername = "localhost";
    private $username = "web";
    private $password = "web";
    private $database = "scandiweb";
    private $connection = NULL;

    public function __construct() {
        $this->connection = new mysqli($this->servername, $this->username, $this->password, $this->database);

        if ($this->connection->connect_error) {
            die('Failed to connect to MySQL - ' . $this->connection->connect_error);
        }
    }

    public function query($query) {
        return $this->connection->query($query);
    }

    public function escapeString($string) {
        return $this->connection::mysqli_real_escape_string($string);
    }

    public function __destruct() {
        $this->connection->close();
    }

}
