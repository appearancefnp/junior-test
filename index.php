<?php

include_once('Route.php');
include_once('Database.php');
include_once('controllers\ProductController.php');
include_once('models\Product.php');

Route::add('/', function() {
    echo 'My home page! /product/list and /product/add is available';
});

Route::add('/product/list', function() {
    ProductController::index();
});

//Delete product(s)
Route::add('/product/delete', function() {
    ProductController::delete();
}, 'post');

//Respond with a form
Route::add('/product/new', function() {
    ProductController::new();
}, 'get');

//Post a new product
Route::add('/product/new', function() {
    ProductController::create();
}, 'post');

Route::run('/');
?>